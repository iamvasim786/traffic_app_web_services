<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <link rel="stylesheet" href="/resources/demos/style.css">
<script type="text/javascript">
	function AddKeyword() {
		var keyword = $('#keyword').val();

		var data0 = {
			keyword : keyword
		};
		$.ajax({
			url : "addkeyword",
			type : "POST",
			data : JSON.stringify(data0),
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			success : function(data) {
				var obj = JSON.parse(data);
			},
			error : function(error) {
				alert("Error !")
			}
		});
	}

	function fetchKeyword() {
		var keyword = $('#show').val();
		if (keyword.length == 3) {
			
			$
					.ajax({
						url : "fetchkeyword",
						type : "GET",
						data : {
							'keyword' : keyword
						},
						success : function(data) {
							var obj = JSON.parse(data);
							var availableTags = [];
							$
									.each(
											obj,
											function(key, value) {
												
												//$("#trafficKeyword")
													//	.append(
														//		"<option value='" + 
			                //value.keyword + "'></option>");
												availableTags.push(value.keyword);
												//alert(availableTags);
											})
							$('#show').autocomplete({
								source : availableTags
							});

						},
						error : function(error) {
							alert("Error !")
						}
					});
		}
		else if (keyword.length<=2){
			$("#trafficKeyword").empty();
		}

	}
</script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Traffic Keyword</title>
</head>
<body>
	<label>Keyword:</label>
	<input type="text" name="keyword" id="keyword">
	<br>
	<br>
	<input type="button" onclick="AddKeyword()" value="AddKeyword">
	<br>

	<br>
	<br>
	<label> Enter Keyword:</label>
	
	
	<br>
	<br>
	<input type="text" id="show" oninput="fetchKeyword()">
</body>
</html>