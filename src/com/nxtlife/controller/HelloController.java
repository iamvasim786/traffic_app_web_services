package com.nxtlife.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nxtlife.model.TrafficKeyword;
import com.nxtlife.service.AddKeywordService;
import com.nxtlife.service.FetchKeywordService;

@Controller
public class HelloController {
	@Autowired
	private AddKeywordService service;
	@Autowired
	private FetchKeywordService fetchService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String Keyword(Model model) {

		return "addkeyword";
	}

	@RequestMapping(value = "addkeyword", method = RequestMethod.POST)
	public @ResponseBody String Add(@RequestBody TrafficKeyword trafficKeyword) throws JsonProcessingException {
		service.addKeyword(trafficKeyword);
		return new ObjectMapper().writeValueAsString(trafficKeyword);
	}

	@RequestMapping(value = "fetchkeyword", method = RequestMethod.GET)
	public @ResponseBody String Add(@RequestParam String keyword) throws JsonProcessingException {
		List<TrafficKeyword> keywords = fetchService.fetchKeyword(keyword);
		System.out.println(keywords.size());
		return new ObjectMapper().writeValueAsString(keywords);
	}

	/*
	 * @RequestMapping(value = "adduser", method = RequestMethod.POST)
	 * public @ResponseBody String Add(@RequestBody User user) throws
	 * JsonProcessingException { service.addUser(user); return new
	 * ObjectMapper().writeValueAsString(user); }
	 */
	@RequestMapping(value = "nextpage", method = RequestMethod.GET)
	public String NxtPage(Model model) {

		return "nextpage";
	}
}
