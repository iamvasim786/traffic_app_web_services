package com.nxtlife.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nxtlife.model.TrafficKeyword;

@Repository
public class KeywordDaoImpl implements KeywordDao {
	@Autowired
	private SessionFactory sessionFactory;

	public void addKeyword(TrafficKeyword trafficKeyword) {
		sessionFactory.getCurrentSession().save(trafficKeyword);
	}

	@SuppressWarnings("unchecked")
	public List<TrafficKeyword> fetchKeyword(String keyword) {
		return sessionFactory.getCurrentSession().createCriteria(TrafficKeyword.class)
				.add(Restrictions.like("keyword",'%'+keyword+'%', MatchMode.ANYWHERE)).list();
	}

}
